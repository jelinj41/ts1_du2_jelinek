package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

public class CalculatorTest {
    Calculator calc;

    @BeforeEach
    public void setup(){
        calc = new Calculator();
    }

    @Test
    public void Add_TwoPlusFour_Six(){
        int a = 2;
        int b = 4;
        int expectedResult = 6;

        int result = calc.add(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Subtract_SixMinusOne_Five(){
        int a = 6;
        int b = 1;
        int expectedResult = 5;

        int result = calc.subtract(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Multiply_ThreeTimesFive_Fifteen(){
        int a = 3;
        int b = 5;
        int expectedResult = 15;

        int result = calc.multiply(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwelveDividedByFour_Three(){
        int a = 12;
        int b = 4;
        int expectedResult = 3;

        int result = calc.divide(a,b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwoDividedByZero_ExceptionThrow(){
        int a = 2;
        int b = 0;

        // int result = calc.divide(a,b);

        Assertions.assertThrows(Exception.class, () -> calc.divide(a,b));
    }
    @AfterEach
    public void closeEach(){

    }

}
